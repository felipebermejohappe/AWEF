﻿using UnityEngine;

public class FadeTransition : MonoBehaviour
{
    public static FadeTransition Instance;

    public enum Options {Start, Next};
    Options actualOption;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
            
    }

    public void TransitionOutSceneChange()
    {
        if (actualOption == Options.Next)
        {
            ScenesDirector.Instance.NextScene();
        }
        else
        {
            ScenesDirector.Instance.StartGame();
        }
    }

    public void SetActualOption(Options selectedOption)
    {
        actualOption = selectedOption;
    }

    public void TurnToFadeOut()
    {
        gameObject.GetComponent<Animator>().SetBool(Parameters.FadeIn, false);
    }

    public void FadeInCompleted()
    {
        if (LevelManager.Instance != null)
        {
            LevelManager.Instance.Ready();
        }
    }
}
