﻿using UnityEngine;

public class FadeSpotRotation : MonoBehaviour
{
    public static FadeSpotRotation Instance;
    
    static int displacement = 0;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void OnEnable()
    {
        RotateFade(Parameters.FadeAction.Opening);
        displacement++;
    }

    public void RotateFade(Parameters.FadeAction fadeAction)
    {
        if (fadeAction == Parameters.FadeAction.Opening)
        {
            transform.Rotate(Vector3.forward, -30 * displacement);
        }
        else
        {
            RotateFade();
        }
    }

    public void RotateFade()
    {
        transform.Rotate(Vector3.forward, -30);
    }

    public void ResetDisplacement()
    {
        displacement = 0;
    }
}
