﻿using UnityEngine;

public class PlayerServe : MonoBehaviour
{
    public static PlayerServe Instance;

    [SerializeField]
    BallMovement ballMovement = null;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ballMovement.Launch();
            gameObject.GetComponent<PlayerHit>().PlayHitSound();
            enabled = false;
        }
    }
}
