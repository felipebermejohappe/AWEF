﻿using UnityEngine;

public class PlayerHit : MonoBehaviour
{
    [SerializeField]
    AudioSource playerHitSound = null;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.layer == 9)
        {
            PlayHitSound();
        }
    }

    public void PlayHitSound()
    {
        playerHitSound.Play();
    }
}
