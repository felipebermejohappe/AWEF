﻿using UnityEngine;

public class BallMovement : MonoBehaviour
{
    [SerializeField]
    Transform origin = null;

    [SerializeField]
    Rigidbody rb = null;

    [SerializeField]
    Vector2 ballDirection = Vector2.one;

    [SerializeField]
    float minHorizontalAngle = 30;
    [SerializeField]
    float minVerticalAngle = 15;

    [SerializeField]
    float ballVelocity = 10;

    bool isLaunched = false;
    bool fixVelocity = false;
    bool fixHorizontal = false;
    bool fixVertical = false;

    const float Angle180 = 180;
    const float Angle90  = 90;

    void Update()
    {
        if (isLaunched)
        {
            if (fixVelocity)
            {
                fixVelocity = false;

                if (fixHorizontal)
                {
                    fixHorizontal = false;
                    FixingHorizontal();
                }
                else if (fixVertical)
                {
                    fixVertical = false;
                    FixingVertical();
                }

                rb.velocity = Vector2.ClampMagnitude(rb.velocity * ballVelocity, ballVelocity);
            }
        }
    }

    public void Launch()
    {
        isLaunched = true;
        transform.SetParent(null);
        rb.isKinematic = false;
        rb.collisionDetectionMode = CollisionDetectionMode.Continuous;
        rb.interpolation = RigidbodyInterpolation.Extrapolate;
        rb.velocity = Vector2.ClampMagnitude(ballDirection * ballVelocity, ballVelocity);
    }

    public void Dead()
    {
        isLaunched = false;
        transform.SetParent(origin);
        transform.localPosition = Vector3.zero;
        rb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
        rb.interpolation = RigidbodyInterpolation.None;
        rb.isKinematic = true;
        StopMovement();

    }

    void StopMovement()
    {
        rb.velocity = Vector2.zero;
        rb.angularVelocity = Vector2.zero;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (isLaunched)
        {
            fixVelocity = true;

            if (rb.velocity.y == 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, 0.5f);
            }

            if (Mathf.Abs(rb.velocity.x) > 2f * Mathf.Abs(rb.velocity.y))
            {
                fixHorizontal = true;
            }
            else if (Mathf.Abs(rb.velocity.y) > 2f * Mathf.Abs(rb.velocity.x))
            {
                fixVertical = true;
            }
        }
    }

    void FixingHorizontal()
    {
        // Comprobamos si hay que corregir angulos muy planos.
        if (Mathf.Atan2(rb.velocity.y, rb.velocity.x) * (Angle180 / Mathf.PI) > 0 &&
            Mathf.Atan2(rb.velocity.y, rb.velocity.x) * (Angle180 / Mathf.PI) < minHorizontalAngle)
        {
            rb.velocity = new Vector2(Mathf.Cos(Mathf.Deg2Rad * minHorizontalAngle), Mathf.Sin(Mathf.Deg2Rad * minHorizontalAngle));
        }
        else if (Mathf.Atan2(rb.velocity.y, rb.velocity.x) * (Angle180 / Mathf.PI) > Angle180 - minHorizontalAngle &&
                 Mathf.Atan2(rb.velocity.y, rb.velocity.x) * (Angle180 / Mathf.PI) < Angle180)
        {
            rb.velocity = new Vector2(Mathf.Cos(Mathf.Deg2Rad * (Angle180 - minHorizontalAngle)), 
                                      Mathf.Sin(Mathf.Deg2Rad * (Angle180 - minHorizontalAngle)));
        }
        else if (Mathf.Atan2(rb.velocity.y, rb.velocity.x) * (Angle180 / Mathf.PI) < -Angle180 + minHorizontalAngle &&
                 Mathf.Atan2(rb.velocity.y, rb.velocity.x) * (Angle180 / Mathf.PI) > -Angle180)
        {
            rb.velocity = new Vector2(Mathf.Cos(Mathf.Deg2Rad * -(Angle180 - minHorizontalAngle)), 
                                      Mathf.Sin(Mathf.Deg2Rad * -(Angle180 - minHorizontalAngle)));
        }
        else if (Mathf.Atan2(rb.velocity.y, rb.velocity.x) * (Angle180 / Mathf.PI) < 0 &&
                 Mathf.Atan2(rb.velocity.y, rb.velocity.x) * (Angle180 / Mathf.PI) > -minHorizontalAngle)
        {
            rb.velocity = new Vector2(Mathf.Cos(Mathf.Deg2Rad * -minHorizontalAngle), Mathf.Sin(Mathf.Deg2Rad * -minHorizontalAngle));
        }
    }

    void FixingVertical()
    {
        // Comprobamos si hay que corregir angulos muy verticales.
        if (Mathf.Atan2(rb.velocity.y, rb.velocity.x) * (Angle180 / Mathf.PI) > Angle90 - minVerticalAngle &&
            Mathf.Atan2(rb.velocity.y, rb.velocity.x) * (Angle180 / Mathf.PI) < Angle90)
        {
            rb.velocity = new Vector2(Mathf.Cos(Mathf.Deg2Rad * (Angle90 - minVerticalAngle)), 
                                      Mathf.Sin(Mathf.Deg2Rad * (Angle90 - minVerticalAngle)));
        }
        else if (Mathf.Atan2(rb.velocity.y, rb.velocity.x) * (Angle180 / Mathf.PI) > Angle90 &&
                 Mathf.Atan2(rb.velocity.y, rb.velocity.x) * (Angle180 / Mathf.PI) < Angle90 + minVerticalAngle)
        {
            rb.velocity = new Vector2(Mathf.Cos(Mathf.Deg2Rad * (Angle90 + minVerticalAngle)), 
                                      Mathf.Sin(Mathf.Deg2Rad * (Angle90 + minVerticalAngle)));
        }
        else if (Mathf.Atan2(rb.velocity.y, rb.velocity.x) * (Angle180 / Mathf.PI) < -Angle90 &&
                 Mathf.Atan2(rb.velocity.y, rb.velocity.x) * (Angle180 / Mathf.PI) > -(Angle90 + minVerticalAngle))
        {
            rb.velocity = new Vector2(Mathf.Cos(Mathf.Deg2Rad * -(Angle90 + minVerticalAngle)), 
                                                Mathf.Sin(Mathf.Deg2Rad * -(Angle90 + minVerticalAngle)));
        }
        else if (Mathf.Atan2(rb.velocity.y, rb.velocity.x) * (Angle180 / Mathf.PI) < -(Angle90 - minVerticalAngle) &&
                 Mathf.Atan2(rb.velocity.y, rb.velocity.x) * (Angle180 / Mathf.PI) > -Angle90)
        {
            rb.velocity = new Vector2(Mathf.Cos(Mathf.Deg2Rad * -(Angle90 - minVerticalAngle)), 
                                      Mathf.Sin(Mathf.Deg2Rad * -(Angle90 - minVerticalAngle)));
        }
    }

    private void OnDisable()
    {
        StopMovement();
    }
}
