﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMaterial : MonoBehaviour
{
    [SerializeField]
    MeshRenderer myMeshRender = null;

    // Start is called before the first frame update
    void Start()
    {
        myMeshRender.material = Parameters.airMaterial;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            myMeshRender.material = Parameters.airMaterial;
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            myMeshRender.material = Parameters.waterMaterial;
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            myMeshRender.material = Parameters.earthMaterial;
        }
        else if (Input.GetKeyDown(KeyCode.F))
        {
            myMeshRender.material = Parameters.fireMaterial;
        }
    }

    public Material GetMaterial()
    {
        return myMeshRender.material;
    }
}
