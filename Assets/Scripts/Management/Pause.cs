﻿using UnityEngine;

public class Pause : MonoBehaviour
{
    [SerializeField]
    GameObject menu = null;

    bool pausable = false;
    bool paused = false;

    // Start is called before the first frame update
    void Start()
    {
        PauseOff(); 

        if (LevelManager.Instance != null)
        {
            pausable = true;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (pausable && Input.GetKeyDown(KeyCode.P))   
        {
            if (!paused)
            {
                PauseOn();
                menu.SetActive(true);
            }
            else
            {
                PauseOff();
                menu.SetActive(false);
            }
        }
    }

    public void PauseOn()
    {
        Time.timeScale = Parameters.FALSE;
        paused = true;
    }

    public void PauseOff()
    {
        Time.timeScale = Parameters.TRUE;
        paused = false;
    }
}
