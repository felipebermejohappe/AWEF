﻿using UnityEngine;

public static class Parameters
{
    // Basic
    public const string On = "On";
    public const string Off = "Off";

    public const float TRUE  = 1;
    public const float FALSE = 0;

    public enum Phase { Main, Game, Credits};

    // Fade
    public enum FadeAction { Opening, Closing };

    // Audio 
    public const string MasterVolume   = "MasterVolume";
    public const string MusicVolume    = "MusicVolume";
    public const string FXSoundsVolume = "FXSoundsVolume";
    public const float  minDB = -60;
    public const float  maxDB = -0;

    // Graphics
    public const string GraphicsState = "GraphicsState";
    public const string dynamicState  = "DynamicState";
    public const string Dynamic       = "Dynamic";
    public const string Static        = "Static";

    // Scores
    public const string highScore     = "highScore";
    public const string lastScore     = "lastScore";

    //Materials
    public static Material airMaterial   = (Material)Resources.Load("Materials/AirMaterial"  , typeof(Material));
    public static Material earthMaterial = (Material)Resources.Load("Materials/EarthMaterial", typeof(Material));
    public static Material fireMaterial  = (Material)Resources.Load("Materials/FireMaterial" ,  typeof(Material));
    public static Material waterMaterial = (Material)Resources.Load("Materials/WaterMaterial", typeof(Material));

    //Animations
    public static string FadeIn = "FadeIn";
}
