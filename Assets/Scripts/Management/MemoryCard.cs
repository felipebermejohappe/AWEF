﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MemoryCard
{
    // Save section
    public static void SaveMasterVolume(float masterVolume)
    {
        PlayerPrefs.SetFloat(Parameters.MasterVolume, masterVolume);
    }

    public static void SaveMusicVolume(float musicVolume)
    {
        PlayerPrefs.SetFloat(Parameters.MusicVolume, musicVolume);
    }

    public static void SaveFXSoundsVolume(float fxSoundsVolume)
    {
        PlayerPrefs.SetFloat(Parameters.FXSoundsVolume, fxSoundsVolume);
    }

    public static void SaveGraphicsState(float graphicsState)
    {
        PlayerPrefs.SetFloat(Parameters.GraphicsState, graphicsState);
    }

    public static void SaveHighScore(int highScore)
    {
        PlayerPrefs.SetInt(Parameters.highScore, highScore);
    }

    public static void SaveLastScore(int lastScore)
    {
        PlayerPrefs.SetInt(Parameters.lastScore, lastScore);
    }

    // Load Section
    public static float LoadMasterVolume()
    {
        return PlayerPrefs.GetFloat(Parameters.MasterVolume, Parameters.maxDB);
    }

    public static float LoadMusicVolume()
    {
        return PlayerPrefs.GetFloat(Parameters.MusicVolume, Parameters.maxDB);
    }

    public static float LoadFXSoundsVolume()
    {
        return PlayerPrefs.GetFloat(Parameters.FXSoundsVolume, Parameters.maxDB);
    }

    public static float LoadGraphicsState()
    {
        return PlayerPrefs.GetFloat(Parameters.GraphicsState, Parameters.TRUE);
    }

    public static int LoadHighScore()
    {
        return PlayerPrefs.GetInt(Parameters.highScore, 0);
    }

    public static int LoadLastScore()
    {
        return PlayerPrefs.GetInt(Parameters.lastScore, 0);
    }
}
