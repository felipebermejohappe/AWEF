﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesDirector : MonoBehaviour
{
    public static ScenesDirector Instance;

    [SerializeField]
    int mainMenuIndex = 0;
    [SerializeField]
    int gameFistScene = 1;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void MainMenuScene()
    {
        BrickHit.ResetHittableBricks();
        FadeSpotRotation.Instance.ResetDisplacement();
        SceneManager.LoadScene(mainMenuIndex);
    }

    public void StartGame()
    {
        SceneManager.LoadScene(gameFistScene);
    }

    public void NextScene()
    {
        int nextSceneIndex = SceneManager.GetActiveScene().buildIndex;

        nextSceneIndex++;

        if (nextSceneIndex == SceneManager.sceneCountInBuildSettings)
        {
            nextSceneIndex = mainMenuIndex;
            BrickHit.ResetHittableBricks();
            FadeSpotRotation.Instance.ResetDisplacement();
        }

        SceneManager.LoadScene(nextSceneIndex);
    }

    public void CloseApp()
    {
        Application.Quit();
    }
}
