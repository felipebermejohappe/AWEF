﻿using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;
    [SerializeField]
    bool lastLevel = false;
    [SerializeField]
    BallMovement bMovement = null;
    [SerializeField]
    PlayerMovement pMovement = null;
    [SerializeField]
    PlayerServe pServe = null;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Ready()
    {
        FreeMovements();   
    }

    public void Completed()
    {
        if (lastLevel)
        {
            ScoreManager.Instance.CloseScore();
        }
        else
        {
            FadeSpotRotation.Instance.RotateFade(Parameters.FadeAction.Closing);
        }
       
        FadeTransition.Instance.SetActualOption(FadeTransition.Options.Next);
        FadeTransition.Instance.TurnToFadeOut();
        FreezeMovements();
    }

    public void FreeMovements()
    {
        bMovement.enabled = true;
        pMovement.enabled = true;
        pServe.enabled    = true;
    }

    public void FreezeMovements()
    {
        bMovement.enabled = false;
        pMovement.enabled = false;
    }


}
