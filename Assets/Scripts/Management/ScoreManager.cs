﻿using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager Instance;

    // Scores in Game
    [SerializeField]
    TMP_Text savedScorePoints = null;
    [SerializeField]
    TMP_Text levelScorePoints = null;
    [SerializeField]
    TMP_Text totalScorePoints = null;
    [SerializeField]
    TMP_Text highScorePoints  = null;
    [SerializeField]
    TMP_Text ComboText = null;

    // Scores in Main Menu
    [SerializeField]
    TMP_Text highScoreText = null;
    [SerializeField]
    TMP_Text lastScoreText = null;

    static int  savedScoreValue = 0;
    static int  levelScoreValue = 0;
    static int  totalScoreValue = 0;
    static int  highScoreValue  = 0;
    static int  lastScoreValue  = 0;
    static int  combo           = 0;
    static bool loadScores      = true;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (LevelManager.Instance == null)
        {
            if (loadScores)
            {
                loadScores = false;
                highScoreValue = MemoryCard.LoadHighScore();
                lastScoreValue = MemoryCard.LoadLastScore();
            }

            highScoreText.text   = highScoreValue.ToString();
            lastScoreText.text = lastScoreValue.ToString();
        }
        else
        {
            Totalize();
            RefreshInGameUI();
        }        
    }

    void Totalize()
    {
        totalScoreValue = savedScoreValue + levelScoreValue;
    }

    public void CommitScore()
    {
        savedScoreValue += levelScoreValue;
        levelScoreValue = 0;
        RefreshSavedScoreUI();
        RefreshLevelScoreUI();
    }

    public void CloseScore()
    {
        lastScoreValue = totalScoreValue;
        MemoryCard.SaveLastScore(lastScoreValue);

        if (totalScoreValue > highScoreValue)
        {
            highScoreValue = totalScoreValue;
            MemoryCard.SaveHighScore(highScoreValue);
        }

        savedScoreValue = 0;
    }

    public void AddToCombo()
    {
        combo++;
        RefreshComboTextUI();
    }

    public void AddLevelScore(int scoreEarned)
    {
        levelScoreValue += (scoreEarned * combo * combo);
        RefreshLevelScoreUI();
        combo = 0;
        RefreshComboTextUI();
        Totalize();
        RefreshTotalScoreUI();
    }

    public void DivideLevelScore()
    {
        levelScoreValue /= 2;
        RefreshLevelScoreUI();
        combo = -1;
        RefreshComboTextUI();
        Totalize();
        RefreshTotalScoreUI();
    }

    void RefreshInGameUI()
    {
        RefreshSavedScoreUI();
        RefreshLevelScoreUI();
        RefreshTotalScoreUI();
        RefreshHighScoreUI(Parameters.Phase.Game);
    }

    void RefreshInMainMenuUI()
    {
        RefreshHighScoreUI(Parameters.Phase.Main);
    }

    void RefreshSavedScoreUI()
    {
        savedScorePoints.text = savedScoreValue.ToString();
    }

    void RefreshLevelScoreUI()
    {
        levelScorePoints.text = levelScoreValue.ToString();
    }

    void RefreshTotalScoreUI()
    {
        totalScorePoints.text = totalScoreValue.ToString();
    }

    void RefreshHighScoreUI(Parameters.Phase phase)
    {
        switch (phase)
        {
            case Parameters.Phase.Main:
                highScoreText.text = highScoreValue.ToString();
                break;
            case Parameters.Phase.Game:
                highScorePoints.text = highScoreValue.ToString();
                break;
        } 
    }

    void RefreshLastScoreUI()
    {
        lastScoreText.text = lastScoreValue.ToString();
    }

    void RefreshComboTextUI()
    {
        if (combo >= 0)
        {
            ComboText.text = "X " + combo.ToString();
        }
        else
        {
            ComboText.text = "DIVIDED";
            combo = 0;
        }
    }

    public void ResetScores()
    {
        savedScoreValue = 0;
        levelScoreValue = 0;
        totalScoreValue = 0;
    }
}
