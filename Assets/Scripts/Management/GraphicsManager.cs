﻿using UnityEngine;
using UnityEngine.UI;

public class GraphicsManager : MonoBehaviour
{
    [SerializeField]
    Material[] arrayDinamicMaterials = null;

    [SerializeField]
    Toggle graphicsStateToggle = null;

    [SerializeField]
    Text graphicsStateText = null;

    float  StateValue;
    string StateText = "";
    bool save = true;

    private void Start()
    {
        save = false;

        if (MemoryCard.LoadGraphicsState() == Parameters.TRUE)
        {
            graphicsStateToggle.isOn =true;
            GraphicsDynamic(true);
        }
        else
        {
            graphicsStateToggle.isOn = false;
            GraphicsDynamic(false);
        }

        save = true;
    }

    public void GraphicsDynamic(bool dynamicState)
    {
        if (dynamicState)
        {
            DynamicValuesSetup();
        }
        else
        {
            StaticValuesSetup();
        }

        for (int index=0; index < arrayDinamicMaterials.Length; index++)
        {
            arrayDinamicMaterials[index].SetFloat(Parameters.dynamicState, StateValue);
        }

        graphicsStateText.text = StateText;

        if (save)
        {
            MemoryCard.SaveGraphicsState(StateValue);
        }
    }

    void DynamicValuesSetup()
    {
        StateValue = Parameters.TRUE;
        StateText = Parameters.Dynamic;
    }

    void StaticValuesSetup()
    {
        StateValue = Parameters.FALSE;
        StateText = Parameters.Static;
    }
}
