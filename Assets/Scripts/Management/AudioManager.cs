﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    [SerializeField]
    AudioMixer audioMixerMaster = null;

    [SerializeField]
    Toggle masterStateToogle = null;

    [SerializeField]
    Text masterStateText = null;

    [SerializeField]
    Slider musicSlider = null;

    [SerializeField]
    Slider fxSoundsSlider = null;

    private void Start()
    {
        if (MemoryCard.LoadMasterVolume() == Parameters.maxDB)
        {
            masterStateToogle.isOn = true;
            SoundOn();
        }
        else
        {
            masterStateToogle.isOn = false;
            SoundOff();
        }

        musicSlider.value = MemoryCard.LoadMusicVolume();
        fxSoundsSlider.value = MemoryCard.LoadFXSoundsVolume();
    }

    public void SetMusicVolume(float musicVolumeValue)
    {
        audioMixerMaster.SetFloat(Parameters.MusicVolume ,musicVolumeValue);
        MemoryCard.SaveMusicVolume(musicVolumeValue);
    }

    public void SetFXSoundsVolume(float fxSoundsVolumeValue)
    {
        audioMixerMaster.SetFloat(Parameters.FXSoundsVolume, fxSoundsVolumeValue);
        MemoryCard.SaveFXSoundsVolume(fxSoundsVolumeValue);
    }

    public void SoundActive(bool isSoundActive)
    {
        if (isSoundActive)
        {
            SoundOn();
            MemoryCard.SaveMasterVolume(Parameters.maxDB);
        }
        else
        {
            SoundOff();
            MemoryCard.SaveMasterVolume(Parameters.minDB);
        }
    }

    void SoundOn()
    {
        audioMixerMaster.SetFloat(Parameters.MasterVolume, Parameters.maxDB);
        masterStateText.text = Parameters.On;
    }

    void SoundOff()
    {
        audioMixerMaster.SetFloat(Parameters.MasterVolume, Parameters.minDB);
        masterStateText.text = Parameters.Off;
    }
}
