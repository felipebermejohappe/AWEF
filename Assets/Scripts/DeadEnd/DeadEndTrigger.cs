﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadEndTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        other.GetComponent<BallMovement>().Dead();
        ScoreManager.Instance.DivideLevelScore();
        PlayerServe.Instance.enabled = true;
    }
}
