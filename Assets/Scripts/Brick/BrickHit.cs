﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickHit : MonoBehaviour
{
    static int hittableBricks = 0;
    static bool resetHittable = true;

    [SerializeField]
    int brickHitPoints = 100;

    [SerializeField]
    BrickMaterial brickMaterial = null;

    [SerializeField]
    BoxCollider brickCollider = null;

    [SerializeField]
    AudioSource successSound = null;

    [SerializeField]
    AudioSource failSound = null;

    private void Awake()
    {
        if (resetHittable)
        {
            resetHittable = false;
            hittableBricks = 0;
        }
        
    }
    private void OnEnable()
    {
        hittableBricks++;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (brickMaterial.GetMaterial().name == other.GetComponent<BallMaterial>().GetMaterial().name)
        {
            successSound.Play();
            brickMaterial.TurnOffRender();
            brickCollider.enabled = false;
            hittableBricks--;
            ScoreManager.Instance.AddToCombo();

            if (hittableBricks == 0)
            {
                resetHittable = true;
                ScoreManager.Instance.AddLevelScore(brickHitPoints);
                ScoreManager.Instance.CommitScore();
                LevelManager.Instance.Completed();
            }
        }
        else
        {
            failSound.Play();
            brickCollider.isTrigger = false;
            ScoreManager.Instance.AddLevelScore(brickHitPoints);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        brickCollider.isTrigger = true;
    }

    public static void ResetHittableBricks()
    {
        resetHittable = true;
    }
}
