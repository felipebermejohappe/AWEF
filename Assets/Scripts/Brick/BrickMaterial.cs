﻿using UnityEngine;

public class BrickMaterial : MonoBehaviour
{
    [SerializeField]
    MeshRenderer myMeshRenderer = null;

    int maxMaterials = 4;

    // Start is called before the first frame update
    void Start()
    {
        switch (Random.Range((int) 0, maxMaterials))
        {
            case 0:
                myMeshRenderer.material = Parameters.airMaterial;
                break;
            case 1:
                myMeshRenderer.material = Parameters.earthMaterial;
                break;
            case 2:
                myMeshRenderer.material = Parameters.fireMaterial;
                break;
            case 3:
                myMeshRenderer.material = Parameters.waterMaterial;
                break;
        }
    }

    public Material GetMaterial()
    {
        return myMeshRenderer.material;
    }

    public void TurnOffRender()
    {
        myMeshRenderer.enabled = false;
    }

}
